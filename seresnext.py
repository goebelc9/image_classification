# -*- coding: utf-8 -*-

import numpy as np
import copy
import random
from random import shuffle
import os
from PIL import Image
from keras.callbacks import ModelCheckpoint
from keras.callbacks import ReduceLROnPlateau
from keras import optimizers
from keras.optimizers import SGD
from keras.layers import Input, Dense, Conv2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Flatten, Activation
from keras.layers.merge import add
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras import backend as K

from keras.preprocessing.image import ImageDataGenerator
import sys
import tensorflow as tf
import keras
from classification_models.keras import Classifiers
from LRFinder import LRFinder
from LRFinder import SGDRScheduler
from keras.utils import multi_gpu_model

sys.setrecursionlimit(3000)

random.seed(123)
BATCH_SIZE = 32
EPOCH = 50
gpus = tf.config.experimental.list_physical_devices('GPU')

def get_file_list_from_dir(datadir):
    included_extensions = ['jpg','jpeg']
    all_files = [j for j in os.listdir(os.path.abspath(datadir)) if any(j.endswith(ext) for ext in included_extensions)]
    data_files = list(all_files)
    return data_files

dir_pro = 'splits/train/protest/'
dir_rd = 'splits/train/non-protest/'
dir_pro_test = 'splits/test/protest/'
dir_rd_test = 'splits/test/non-protest/'
dir_pro_eval = 'splits/eval/protest/'
dir_rd_eval = 'splits/eval/non-protest/'
dir_test = 'splits/test'
dir_eval = 'splits/eval'
dir_train = 'splits/train'


protest_train = get_file_list_from_dir(dir_pro)
random_train = get_file_list_from_dir(dir_rd)
protest_test = get_file_list_from_dir(dir_pro_test)
random_test = get_file_list_from_dir(dir_rd_test)
protest_eval = get_file_list_from_dir(dir_pro_test)
random_eval = get_file_list_from_dir(dir_rd_test)


# load data
train_datagen = ImageDataGenerator(rescale = 1./255, rotation_range = 45, width_shift_range = 0.2, height_shift_range = 0.2, zoom_range = 0.2, shear_range = 0.2, horizontal_flip = True, vertical_flip = True, fill_mode = 'nearest')

test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory(dir_train,
target_size = (224, 224),
batch_size = BATCH_SIZE,
class_mode = 'binary')

test_set = test_datagen.flow_from_directory(dir_test,
target_size = (224, 224),
batch_size = BATCH_SIZE,
class_mode = 'binary')


#configure model
SeResNext101, preprocess_input = Classifiers.get ('seresnext101')
base_model = SeResNext101(input_shape=(224,224,3), weights = None, include_top = False)
x = keras.layers.GlobalAveragePooling2D()(base_model.output)
#x = Flatten()(x)
output = keras.layers.Dense(1, activation="sigmoid", name='fc1000')(x)


#with strategy.scope():
with tf.device("/cpu:0"):
    model = keras.models.Model (inputs=[base_model.input], outputs = [output])
#model.load_weights("banners_round_8_19-0.92.hdf5")
#model = multi_gpu_model (model, gpus=2)


#sgd = optimizers.SGD(lr=0.1, momentum=0.9, nesterov=False)
adam = keras.optimizers.Adam(learning_rate=0.001)#, beta_1=0.9, beta_2=0.999)

#adam = keras.optimizers.Adam(learning_rate=1e-6)#, beta_1=0.9, beta_2=0.999)
model.compile(optimizer = adam, loss = 'binary_crossentropy', metrics = ['accuracy'])

#model.compile(optimizer=sgd, loss='binary_crossentropy', metrics=['accuracy'])
#model.compile(optimizer='SGD', loss = 'binary_crossentropy', momentum=0.9, lr=0.1, wd=0.0001, nesterov=True, metrics=['accuracy'])


#establish optimum learning rate

"""
lr_finder = LRFinder(min_lr=1e-5,
                    max_lr=1e-2,
                    steps_per_epoch = (len(protest_train)+len(random_train)) // BATCH_SIZE,
                    epochs=3)
model.fit_generator(training_set, test_set, callbacks = [lr_finder])
lr_finder.plot_loss()
sys.exit()
"""


schedule = SGDRScheduler(min_lr=1e-5, 
        max_lr=1e-2,
        steps_per_epoch = (len(protest_train)+len(random_train)) // BATCH_SIZE, 
        lr_decay=0.9,
        cycle_length=5,
        mult_factor=1.5)


#checkpoints
#filepath = "banners_round_9_take_3_{epoch:02d}-{val_accuracy:.2f}.hdf5"
#filepath = "banners_round_8_take_2_{epoch:02d}-{val_accuracy:.2f}.hdf5"
reduce_lr = ReduceLROnPlateau(monitor='val_accuracy', factor=0.1,
                              patience=5, min_lr=1e-6, verbose=1)


#banners_201119_2_1_LR_scheduler31-0.92.hdf5
filepath = "banners_201119_3_1_LR_scheduler{epoch:02d}-{val_accuracy:.2f}.hdf5"
checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', verbose = 1, save_best_only = True, mode = 'max')
callbacks_list = [checkpoint, schedule]
callbacks_list = [checkpoint, reduce_lr]
#callbacks_list = [checkpoint]

# Fit modeli
size_train_sample = len(protest_train)+len(random_train) 
size_test_sample = len(protest_test)+len(random_test)
print ("size of training set:", size_train_sample, "size of testset", size_test_sample)


#model.load_weights("./banners_201119_3_1_LR_scheduler84-0.92.hdf5")
model.fit_generator(training_set, steps_per_epoch = np.ceil(size_train_sample/BATCH_SIZE), epochs = EPOCH, validation_data = test_set, validation_steps = np.ceil (size_test_sample/BATCH_SIZE), callbacks = callbacks_list)

#model.load_weights("banners_201119_2_LR_scheduler86-0.82.hdf5")#banners_round_8_19-0.92.hdf5")
# Validation
#eval_datagen = ImageDataGenerator(rescale = 1./255)
#eval_set = eval_datagen.flow_from_directory(dir_eval,
#target_size = (224, 224),
#batch_size = BATCH_SIZE,
#class_mode = 'binary')

#score = model.evaluate_generator(eval_set, steps=(len(protest_eval)+len(random_eval)) // BATCH_SIZE)
#print("Evaluation Accuracy = ", score[1])
